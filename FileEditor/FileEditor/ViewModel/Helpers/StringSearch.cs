﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace FileEditor.ViewModel.Helpers
{
    public class StringSearch
    {
        public static int[] getTextSearchIndexes(string str, string substr, bool ignoreCase = true)
        {

            var indexes = new List<int>();
            int index = 0;

            if (string.IsNullOrWhiteSpace(str) ||
                string.IsNullOrWhiteSpace(substr))
            {
                return indexes.ToArray();
            }


            while ((index = str.IndexOf(substr, index, ignoreCase ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal)) != -1)
            {
                indexes.Add(index++);
            }

            return indexes.ToArray();
        }

        public static Tuple<int[], string[]> getRegexSearchIndexes(string text, string expr)
        {
            var indexes = new List<int>();
            var str_matches = new List<string>();

            if (string.IsNullOrWhiteSpace(text) ||
                string.IsNullOrWhiteSpace(expr))
            {
                return Tuple.Create(indexes.ToArray(), str_matches.ToArray());
            }

            //Console.WriteLine("The Expression: " + expr);
            MatchCollection mc = Regex.Matches(text, expr);

            foreach (Match m in mc)
            {
                indexes.Add(m.Index);
                str_matches.Add(m.Value);
                //Console.WriteLine(m);
            }
            return Tuple.Create(indexes.ToArray(), str_matches.ToArray());
        }

        public static string ToLiteral(string input)
        {
            using (var writer = new StringWriter())
            {
                using (var provider = CodeDomProvider.CreateProvider("CSharp"))
                {
                    provider.GenerateCodeFromExpression(new CodePrimitiveExpression(input), writer, null);
                    return writer.ToString();
                }
            }
        }
    }
}
