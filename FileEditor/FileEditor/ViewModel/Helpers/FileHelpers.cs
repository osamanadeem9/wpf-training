﻿using FileEditor.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileEditor.ViewModel.Helpers
{
    public class FileHelpers
    {
        public const String storageFileName = @"C:\Users\lmkr\Downloads\sample files\hello.txt";
        public static void appendToFile( string textString, string myfile = storageFileName)
        {
            using (StreamWriter sw = File.AppendText(myfile))
            {                
                sw.WriteLine(textString);    
            }

        }

        public static void clearFile(string myfile = storageFileName)
        {
            File.WriteAllText(storageFileName, string.Empty);
            
        }

        public static List<FileModel> readFileNamesFromFile (string myfile = storageFileName)
        {
            try
            {
                List<FileModel> filesList = new List<FileModel>();

                if (File.Exists(myfile))
                {
                    using (StreamReader sr = new StreamReader(myfile))
                    {
                        while (sr.Peek() >= 0)
                        {
                            string fileName = sr.ReadLine();
                            filesList.Add(new FileModel(fileName, File.ReadAllText(fileName), new System.IO.FileInfo(fileName).Length));
                        }
                    }

                }
                return filesList;

            }catch (Exception e)
            {
                return new List<FileModel>();
            }

        }


    }
}
