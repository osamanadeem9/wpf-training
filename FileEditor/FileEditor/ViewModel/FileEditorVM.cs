﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using FileEditor.ViewModel.Commands;
using FileEditor.Model;
using System.IO;
using FileEditor.ViewModel.Helpers;

namespace FileEditor.ViewModel
{
    public class FileEditorVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public OpenFileCommand OpenFileCommand { get; set; }

        public FileModel selectedFile;

        public FileModel SelectedFile
        {
            get { return selectedFile; }
            set
            {
                selectedFile = value;
                onPropertyChanged("SelectedFile");
                pointerPosition = 0;

            }
        }

        public bool regexCheckBoxStatus;

        public bool RegexCheckBoxStatus
        {
            get { return regexCheckBoxStatus; }
            set
            {
                regexCheckBoxStatus = value;
                onPropertyChanged("RegexCheckBoxStatus");
                pointerPosition = 0;

            }
        }

        public OpenMultipleFilesCommand OpenMultipleFilesCommand { get; set; }
        public OpenFolderCommand OpenFolderCommand { get; set; }
        public FindButtonCommand FindButtonCommand { get; set; }
        public ReplaceButtonCommand ReplaceButtonCommand { get; set; }
        public SaveFileCommand SaveFileCommand { get; set; }


        public string findText_TextBox; 
        public string FindText_TextBox
        {
            get { return findText_TextBox; }
            set { 
                findText_TextBox = value;
                onPropertyChanged("FindText_TextBox");
                pointerPosition = 0;
            }
        }

        public string replaceText_TextBox;
        public string ReplaceText_TextBox
        {
            get { return replaceText_TextBox; }
            set
            {
                replaceText_TextBox = value;
                onPropertyChanged("ReplaceText_TextBox");
                
            }
        }

        public int pointerPosition { get; set; }



        public ObservableCollection<FileModel> FilesList { get; set; }

        public FileEditorVM()
        {
            OpenFileCommand = new OpenFileCommand(this);
            OpenMultipleFilesCommand = new OpenMultipleFilesCommand(this);
            OpenFolderCommand = new OpenFolderCommand(this);
            FindButtonCommand = new FindButtonCommand(this);
            ReplaceButtonCommand = new ReplaceButtonCommand(this);
            SaveFileCommand = new SaveFileCommand(this);
            FilesList = new ObservableCollection<FileModel>();

            regexCheckBoxStatus = false;
            pointerPosition = 0;

            //String path = @"C:\Users\lmkr\Downloads\sample files\was - 1kb.txt";
            //FilesList.Add(new FileModel(path, File.ReadAllText(path), new System.IO.FileInfo(path).Length));

            foreach (var file in FileHelpers.readFileNamesFromFile())
                FilesList.Add(file);

        }

        //public string getFileText()
        //{
        //    return SelectedFile.FileText;
        //}

        //public string getFindText()
        //{
        //    return FindText_TextBox;
        //}

        //public long getPointerPosition()
        //{
        //    return pointerPosition;
        //}
        public void loadFilesToListView (List<FileModel> files)
        {
            FilesList.Clear();

            foreach (var file in files)
            {
                FilesList.Add(file);
            }
        }

        public void onPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged!=null)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}
