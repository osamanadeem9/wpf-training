﻿using FileEditor.ViewModel.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FileEditor.ViewModel.Commands
{
    public class ReplaceButtonCommand : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public FileEditorVM VM { get; set; }
        public ReplaceButtonCommand(FileEditorVM vm)
        {
            VM = vm;
        }

        public bool CanExecute(object parameter)
        {
            if (VM.SelectedFile == null || VM.ReplaceText_TextBox == string.Empty)
                return false;
            else
                return true;
        }

        public void Execute(object parameter)
        {
            TextBox fileTextBox = parameter as TextBox;

            //fileTextBox.Focus();
            //fileTextBox.Select(0, fileTextBox.Text.Length);

            //MessageBox.Show(VM.FindText_TextBox);

            bool regex_status = VM.RegexCheckBoxStatus;

            int[] indexes;
            string[] str_matches = new string[2];
            if (regex_status == false)
            {
                indexes = StringSearch.getTextSearchIndexes(fileTextBox.Text, VM.FindText_TextBox);
            }
            else
            {
                var regex_lists = StringSearch.getRegexSearchIndexes(fileTextBox.Text, VM.FindText_TextBox);
                indexes = regex_lists.Item1;
                str_matches = regex_lists.Item2;
            }

            if (indexes.Length == 0)
            {
                MessageBox.Show(String.Format("Sorry, can't find the word \"{0}\"", VM.FindText_TextBox));

            }
            else
            {
                for (int i = 0; i < indexes.Length; i++)
                {
                    if (indexes[i] > VM.pointerPosition)
                    {
                        VM.pointerPosition = indexes[i];

                        fileTextBox.Focus();
                        //fileTextBox.Select(item, (item + VM.FindText_TextBox.Length-2));
                        
                        fileTextBox.SelectionStart = VM.pointerPosition;
                        fileTextBox.SelectionLength = VM.FindText_TextBox.Length;
                        if (regex_status)
                           fileTextBox.SelectionLength = str_matches[i].Length;

                        fileTextBox.SelectedText = VM.ReplaceText_TextBox;

                        fileTextBox.SelectionStart = VM.pointerPosition;
                        fileTextBox.SelectionLength = VM.ReplaceText_TextBox.Length;

                        if (i == indexes.Length - 1)
                        {
                            MessageBox.Show(String.Format("Reached the end of file. Can't find any more occurences of \"{0}\"", VM.FindText_TextBox));
                            VM.pointerPosition = 0;

                        }
                        break;
                    }

                }
            }
        }
    }
}
