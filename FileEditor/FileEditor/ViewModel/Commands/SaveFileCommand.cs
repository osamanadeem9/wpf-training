﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FileEditor.ViewModel.Commands
{
    public class SaveFileCommand : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        public FileEditorVM VM { get; set; }
        public SaveFileCommand (FileEditorVM vm)
        {
            VM = vm;
        }

        public bool CanExecute(object parameter)
        {
            if (VM.SelectedFile == null)
            {
                return false;
            } else
            {
                return true;
            }
        }

        public void Execute(object parameter)
        {
            TextBox fileTextBox = parameter as TextBox;
            Task writeTask = WriteFileAsync(fileTextBox.Text, VM.SelectedFile.FileName);

            writeTask.Wait();

            if (writeTask.IsCompleted)
            {
                MessageBox.Show(String.Format("File saved successfully to {0}", VM.SelectedFile.FileName));
            }
        }

        public async Task WriteFileAsync(String text, String filePath)
        {
            await File.WriteAllTextAsync(filePath, text);
        }
    }
}
