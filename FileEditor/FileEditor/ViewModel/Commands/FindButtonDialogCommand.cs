﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FileEditor.ViewModel.Commands
{
    public class FindButtonDialogCommand : ICommand
    {
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public FileEditorVM VM { get; set; }

        public FindButtonDialogCommand(FileEditorVM vm)
        {
            VM = vm;
        }
        public bool CanExecute(object parameter)
        {
            //String msg = parameter as String;
            var param = (Tuple<object, object>)parameter;


            TextBox param2 = (TextBox)param.Item2;
            string msg = param2.Text;

            if (msg == string.Empty || msg==null)
            {
                return false;
            } else
            {
                return true;
            }
            //return true;

        }

        public void Execute(object parameter)
        {
            var param = (Tuple<object, object>)parameter;

            string param1 = (string)param.Item1;
            TextBox param2 = (TextBox)param.Item2;

            //String msg = parameter as String;
            MessageBox.Show("You said: " + param2.Text);

        }
    }
}
