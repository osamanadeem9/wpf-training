﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using Microsoft.Win32;
using System.Windows.Input;
using System.Windows.Forms;
using System.IO;
using FileEditor.Model;
using FileEditor.ViewModel.Helpers;

namespace FileEditor.ViewModel.Commands
{
    public class OpenFolderCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public FileEditorVM VM { get; set; }

        public OpenFolderCommand (FileEditorVM vm)
        {
            VM = vm;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    
                    string[] files = Directory.GetFiles(fbd.SelectedPath);
                    var txtFiles = from file in files
                                     let extension = Path.GetExtension(file)
                                     where extension.Equals(".txt") || extension.Equals(".cs")
                                     select file;

                    //System.Windows.MessageBox.Show("Number of Files: " + txtFiles.Count());

                    List<FileModel> filesList = new List<FileModel>();

                    if (File.Exists(FileHelpers.storageFileName))
                        FileHelpers.clearFile();

                    foreach (String fileName in txtFiles)
                    {
                        string fileText = File.ReadAllText(fileName);
                        long fileSize = new System.IO.FileInfo(fileName).Length;
                        filesList.Add(new FileModel(fileName, fileText, fileSize));

                        FileHelpers.appendToFile(fileName);
                    }

                    VM.loadFilesToListView(filesList);
                }
            }
        }
    }
}
