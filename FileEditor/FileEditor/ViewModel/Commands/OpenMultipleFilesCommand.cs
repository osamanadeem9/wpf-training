﻿using FileEditor.Model;
using FileEditor.ViewModel.Helpers;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace FileEditor.ViewModel.Commands
{
    public class OpenMultipleFilesCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public FileEditorVM VM { get; set; }

        public OpenMultipleFilesCommand (FileEditorVM vm)
        {
            VM = vm;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            //TextBox param = parameter as TextBox;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text Files (*.txt)|*.txt";
            //openFileDialog.Filter = "Text Files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.Multiselect = true;



            if (openFileDialog.ShowDialog() == true)
            {
                //param.Text = string.Empty;
                //param.Text = File.ReadAllText(openFileDialog.FileName);

                List<FileModel> filesList = new List<FileModel>();

                if (File.Exists(FileHelpers.storageFileName))
                    FileHelpers.clearFile();

                foreach (String fileName in openFileDialog.FileNames)
                {
                    string fileText = File.ReadAllText(fileName);
                    long fileSize = new System.IO.FileInfo(fileName).Length;
                    filesList.Add(new FileModel(fileName, fileText, fileSize));

                    FileHelpers.appendToFile(fileName);
                }

                VM.loadFilesToListView(filesList);

                //param.Text = string.Empty;
                //param.Text = File.ReadAllText(filesList.FirstOrDefault().FileName);

                //using (FileStream fs = File.Open(openFileDialog.FileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                //using (BufferedStream bs = new BufferedStream(fs))
                //using (StreamReader sr = new StreamReader(bs))
                //{
                //    string line;
                //    while ((line = sr.ReadLine()) != null)
                //    {
                //       // result.AppendLine(line);
                //        param.AppendText(line+"\n");
                //    }
                //}


                //using (StreamReader sr = File.OpenText(openFileDialog.FileName))
                //{
                //    string s = String.Empty;
                //    while ((s = sr.ReadLine()) != null)
                //    {
                //        param.AppendText(s + "\n");
                //    }
                //}
            }





        }
    }
}
