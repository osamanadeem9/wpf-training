﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileEditor.Model
{
    public class FileModel
    {
        public string FileName { get; set; }

        public string FileText { get; set; }

        public long FileSize { get; set; } 

        public FileModel(String filename,  String fileText, long fileSize)
        {
            FileName = filename;
            FileText = fileText;
            FileSize = fileSize;
        }

    }
}
