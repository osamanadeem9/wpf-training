﻿using MyPracticeMVVM.Model;
using MyPracticeMVVM.ViewModel.Commands;
using MyPracticeMVVM.ViewModel.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace MyPracticeMVVM.ViewModel
{
    public class WeatherVM : INotifyPropertyChanged
    {
        private string query;

        public string Query
        {
            get { return query; }
            set
            {
                query = value;
                OnPropertyChanged("Query");                             
            }
        }

        public ObservableCollection<City> Cities { get; set; }

        private CurrentConditions currentConditions;
        public CurrentConditions CurrentConditions
        {
            get { return currentConditions; }
            set
            {
                currentConditions = value;
                OnPropertyChanged("CurrentConditions");
            }
        }

        private City selectedCity;
        public City SelectedCity
        {
            get { return selectedCity; }
            set
            {
                selectedCity = value;
                OnPropertyChanged("SelectedCity");
                GetCurentConditions();
            }
        }

        public SearchCommand SearchCommand { get; set; }
        public WeatherVM()
        {
           if (DesignerProperties.GetIsInDesignMode(new System.Windows.DependencyObject())){
                SelectedCity = new City
                {
                    LocalizedName = "New York"
                };

                CurrentConditions = new CurrentConditions
                {
                    WeatherText = "Cloudy",
                    Temperature = new Temperature
                    {
                        Metric = new Units
                        {
                            Value = "21"
                        }
                    }
                };
            }   
            SearchCommand = new SearchCommand(this);
            Cities = new ObservableCollection<City>();

        }

        public async void GetCurentConditions()
        {
            Query = string.Empty;
            Cities.Clear();
            CurrentConditions = await AccuWeatherHelper.GetCurrentConditions(SelectedCity.Key);
        }
        public async void makeQuery()
        {
            var cities = await AccuWeatherHelper.GetCities(Query);
            Cities.Clear();

            foreach (var city in cities)
            {
                Cities.Add(city); 
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged (String propertyName)
        {
             PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
             
        }
    }
}
