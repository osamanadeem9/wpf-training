﻿using MyPracticeMVVM.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyPracticeMVVM.ViewModel.Helpers
{
    public class AccuWeatherHelper
    {
        public const String BASE_URL = "http://dataservice.accuweather.com/";
        public const String AUTOCOMPLETE_ENDPOINT = "locations/v1/cities/autocomplete?apikey={0}&q={1}";
        public const String CURRENT_CONDITIONS_ENDPOINT = "currentconditions/v1/{0}?apikey={1}";
        public const String API_KEY = "eOb1vZChyaRiq3k4EIWl46csWSlgLioN";

        public static async Task<List<City>> GetCities(String query)
        {
            List<City> cities = new List<City>();

            String url = BASE_URL + String.Format(AUTOCOMPLETE_ENDPOINT, API_KEY, query);

            using (HttpClient client = new HttpClient())
            {
                var response = await client.GetAsync(url);
                String json = await response.Content.ReadAsStringAsync();

                cities = JsonConvert.DeserializeObject<List<City>>(json);

            }

            return cities;
        }

        public static async Task<CurrentConditions> GetCurrentConditions(String cityKey)
        {
            CurrentConditions currentConditions = new CurrentConditions();

            String url = BASE_URL + String.Format(CURRENT_CONDITIONS_ENDPOINT, cityKey, API_KEY);

            using (HttpClient client = new HttpClient())
            {
                var response = await client.GetAsync(url);
                String json = await response.Content.ReadAsStringAsync();

                currentConditions = (JsonConvert.DeserializeObject<List<CurrentConditions>>(json)).FirstOrDefault();


            }

            return currentConditions;
        }

    }
}
